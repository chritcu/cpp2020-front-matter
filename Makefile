.PHONY: once pulp clean

PDFLATEX = pdflatex -synctex=-1

DATE = "$(shell date +'%Y-%m-%d--%H-%M')"

NAME = front_matter

once:  $(NAME).tex
	-$(PDFLATEX) -halt-on-error $<
	-bibtex $(NAME)
	cat $(NAME).pdf > temp.pdf

# You can get pulp here: https://github.com/dmwit/pulp
PULP = "$(shell which pulp || echo \"$(HOME)\"/.cabal/bin/pulp)"

pulp: $(NAME).tex
	-$(PDFLATEX) -interaction nonstopmode $< > /dev/null
	-$(PULP) $(NAME).log

clean:
	@ rm -rf temp *.aux *.log $(NAME).pdf temp.pdf
	@ rm -rf *~ *.log *.out *.bbl *.blg *.brf *.synctex* *.toc
